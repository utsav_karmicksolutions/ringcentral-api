<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\RingCentral\RingCentralWrapper;



class MeetingController extends Controller
{
    private  $ringCentral = null;

    public function __construct()
    {
        $this->ringCentral = new RingCentralWrapper();
    }

    public function index()
    {
        //

        $url = $this->ringCentral->getAuthUrl();
        return view('meeting.index', [
            'url' => $url
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function callback(Request $request)
    {
        $this->ringCentral->set_authCode($request->getQueryString());
        if ($this->ringCentral->authorize() == false) {
            return redirect('/meeting');
        } else {
            return view('meeting.list');
        }
    }

    public function instantMeeting()
    {
        return view('meeting.instantmeeting');
    }

    public function createInstantMeeting(Request $request)
    {
        $topic = $request->input('topic');
        $password = $request->input('password');
        $params = [
            'topic' => $topic,
            'meetingType' => 'Instant',
            'password' => $password,
            'allowJoinBeforeHost' => false,
            'startHostVideo' => false,
            'startParticipantsVideo' => true,

        ];

        $resp = $this->ringCentral->callPostRequest('/restapi/v1.0/account/~/extension/~/meeting', $params);
        $json = [
            'uri' => $resp->json()->uri,
            'id' => $resp->json()->id,
            'topic' => $resp->json()->topic,
            'meetingType' => $resp->json()->meetingType,
            'password' => $resp->json()->password,
            'joinURL' => $resp->json()->links->startUri,
            'startURL' => $resp->json()->links->joinUri
        ];
        dd($json);
    }

    public function testGet()
    {
        $resp = $this->ringCentral->callGetRequest('/rcvideo/v1/history/meetings', []);
        dd($resp->json());
    }

    public function testPost()
    {
        $params = [
            'name' => 'Test Meeting',
            'allowJoinBeforeHost' => true,
            'muteAudio' => false,
            'muteVideo' => true
        ];
        $resp = $this->ringCentral->callPostRequest('/rcvideo/v1/bridges', $params);
        dd($resp->json());
    }
}