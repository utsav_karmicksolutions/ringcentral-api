<?php

namespace App\Http\RingCentral;

use Exception;
use Illuminate\Support\Facades\Session;

class RingCentralWrapper
{

    private $RINGCENTRAL_CLIENTID;
    private $RINGCENTRAL_CLIENTSECRET;
    private $RINGCENTRAL_SERVER;
    private $RINGCENTRAL_USERNAME;
    private $RINGCENTRAL_PASSWORD;
    private $RINGCENTRAL_EXTENSION;
    private $RINGCENTRAL_REDIRECT_URL;
    private $accessToken;
    private $authCode;

    public function __construct()
    {
        $this->RINGCENTRAL_CLIENTID = env('RINGCENTRAL_CLIENTID');
        $this->RINGCENTRAL_CLIENTSECRET = env('RINGCENTRAL_CLIENTSECRET');
        $this->RINGCENTRAL_USERNAME = env('RINGCENTRAL_USERNAME');
        $this->RINGCENTRAL_PASSWORD = env('RINGCENTRAL_PASSWORD');
        $this->RINGCENTRAL_SERVER = env('RINGCENTRAL_SERVER');
        $this->RINGCENTRAL_EXTENSION = env('RINGCENTRAL_EXTENSION');
        $this->RINGCENTRAL_REDIRECT_URL = env('RINGCENTRAL_REDIRECT_URL');
        $this->accessToken = null;
        $this->authCode = null;
    }

    public function get_accountId()
    {
        return $this->RINGCENTRAL_CLIENTID;
    }

    public function get_extension()
    {
        return $this->RINGCENTRAL_EXTENSION;
    }

    public function getAuthUrl()
    {
        $rcsdk = new \RingCentral\SDK\SDK($this->RINGCENTRAL_CLIENTID, $this->RINGCENTRAL_CLIENTSECRET, $this->RINGCENTRAL_SERVER);
        $platform = $rcsdk->platform();

        // Using the authUrl to call the platform function
        $url = $platform->authUrl(array(
            'redirectUri' => $this->RINGCENTRAL_REDIRECT_URL,
            'state' => 'initialState',
            'brandId' => '',
            'display' => '',
            'prompt' => ''
        ));

        return $url;
    }

    public function set_authCode($authCode)
    {
        $this->authCode = $authCode;
    }

    public function authorize()
    {
        if ($this->authCode == null) {
            return false;
        }

        $rcsdk = new \RingCentral\SDK\SDK($this->RINGCENTRAL_CLIENTID, $this->RINGCENTRAL_CLIENTSECRET, $this->RINGCENTRAL_SERVER);
        $platform = $rcsdk->platform();
        try {
            $qs = $platform->parseAuthRedirectUrl($this->authCode);
            $qs["redirectUri"] = $this->RINGCENTRAL_REDIRECT_URL;
            $platform->login($qs);
            $this->accessToken = $platform->auth()->data();
            Session::put('accessToken', $this->accessToken);
            $platform->auth()->setData($this->accessToken);

            if ($platform->loggedIn() == false) {
                return false;
            }
            return true;
        } catch (Exception $ex) {
            return "An error occurred: " . $ex->getMessage();
        }
    }

    public function callGetRequest($endpoint, $params)
    {
        $rcsdk = new \RingCentral\SDK\SDK($this->RINGCENTRAL_CLIENTID, $this->RINGCENTRAL_CLIENTSECRET, $this->RINGCENTRAL_SERVER);
        $platform = $rcsdk->platform();
        $this->accessToken = Session::get('accessToken');
        $platform->auth()->setData($this->accessToken);

        if ($platform->loggedIn() == false) {
            return false;
        }

        try {
            $resp = $platform->get(env('RINGCENTRAL_SERVER') . $endpoint, $params);
            return $resp;
        } catch (\RingCentral\SDK\Http\ApiException $e) {
            return 'Expected HTTP Error: ' . $e->getMessage();
        }
    }

    public function callPostRequest($endpoint, $params)
    {
        $rcsdk = new \RingCentral\SDK\SDK($this->RINGCENTRAL_CLIENTID, $this->RINGCENTRAL_CLIENTSECRET, $this->RINGCENTRAL_SERVER);
        $platform = $rcsdk->platform();
        $this->accessToken = Session::get('accessToken');
        $platform->auth()->setData($this->accessToken);

        if ($platform->loggedIn() == false) {
            return false;
        }

        try {
            $resp = $platform->post(env('RINGCENTRAL_SERVER') . $endpoint, $params);
            return $resp;
        } catch (\RingCentral\SDK\Http\ApiException $e) {
            return 'Expected HTTP Error: ' . $e->getMessage();
        }
    }

    public function callPutRequest($endpoint, $params)
    {
        $rcsdk = new \RingCentral\SDK\SDK($this->RINGCENTRAL_CLIENTID, $this->RINGCENTRAL_CLIENTSECRET, $this->RINGCENTRAL_SERVER);
        $platform = $rcsdk->platform();
        $this->accessToken = Session::get('accessToken');
        $platform->auth()->setData($this->accessToken);

        if ($platform->loggedIn() == false) {
            return false;
        }

        try {
            $resp = $platform->put(env('RINGCENTRAL_SERVER') . $endpoint, $params);
            return $resp;
        } catch (\RingCentral\SDK\Http\ApiException $e) {
            return 'Expected HTTP Error: ' . $e->getMessage();
        }
    }

    public function callDeleteRequest($endpoint, $params)
    {
        $rcsdk = new \RingCentral\SDK\SDK($this->RINGCENTRAL_CLIENTID, $this->RINGCENTRAL_CLIENTSECRET, $this->RINGCENTRAL_SERVER);
        $platform = $rcsdk->platform();
        $this->accessToken = Session::get('accessToken');
        $platform->auth()->setData($this->accessToken);

        if ($platform->loggedIn() == false) {
            return false;
        }

        try {
            $resp = $platform->delete(env('RINGCENTRAL_SERVER') . $endpoint, $params);
            return $resp;
        } catch (\RingCentral\SDK\Http\ApiException $e) {
            return 'Expected HTTP Error: ' . $e->getMessage();
        }
    }
}