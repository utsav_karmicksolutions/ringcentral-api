<?php

use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/meeting', 'MeetingController@index');
Route::get('/authringcentral', 'MeetingController@callback');
Route::get('/instantmeeting', 'MeetingController@instantMeeting')->name('instantmeeting');
Route::get('/scheduledmeeting', 'MeetingController@scheduledMeeting')->name('scheduledmeeting');
Route::post('/createinstantmeeting', 'MeetingController@createInstantMeeting')->name('createInstantMeeting');
Route::post('/scheduledmeeting', 'MeetingController@scheduledMeeting')->name('scheduledmeeting');
Route::get('/testget', 'MeetingController@testGet');
Route::get('/testpost', 'MeetingController@testPost');