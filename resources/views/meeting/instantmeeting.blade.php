@extends('layouts.app')
@section('content')
<div class="page-header">
    <h1>RingCentral - <small>Functions</small></h1>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <form action="{{route('createInstantMeeting')}}" method="POST" role="form">
            {{ csrf_field() }}
            <legend>Create Instant Meeting</legend>

            <div class="form-group">
                <label for="">Meeting Topic</label>
                <input type="text" class="form-control" name="topic" placeholder="Meeting Topic">
                <label for="">Meeting Password</label>
                <input type="text" class="form-control" name="password" placeholder="Meeting Password">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection
