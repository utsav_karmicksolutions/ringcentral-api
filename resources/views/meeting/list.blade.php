@extends('layouts.app')
@section('content')

<div class="page-header">
  <h1>RingCentral - <small>Functions</small></h1>
</div>
<div class="row">
	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		<div class="panel panel-default">
			<div class="panel-body">
				<a href="{{route('instantmeeting')}}">Create Instant Meeting</a>
			</div>
		</div>
	</div>

	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		<div class="panel panel-default">
			<div class="panel-body">
				<a href="{{route("scheduledmeeting")}}">Create Scheduled Meeting</a>
			</div>
		</div>
	</div>
</div>



@endsection
