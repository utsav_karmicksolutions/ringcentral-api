@extends('layouts.app')
@section('content')
<div class="jumbotron">
	<div class="container">
		<h1>RingCentral Authorization Code Flow Authentication</h1>
		<p>Authenticate Ring Central App.</p>
		<p>
			<a href="{{$url}}">Authorize RingCentral Account</a>
		</p>
	</div>
</div>
@endsection
